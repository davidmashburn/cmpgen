from distutils.core import setup

setup(
    name='cmpGen',
    version='0.1.2', # final version, now deprecated
    author='David N. Mashburn',
    author_email='david.n.mashburn@gmail.com',
    packages=['cmpGen'],
    scripts=[],
    url='http://pypi.python.org/pypi/cmpGen/',
    license='LICENSE.txt',
    description='cmpGen is deprecated; it is maintained here for legacy purposes',
    long_description=open('README.rst').read(),
    install_requires=[],
)
